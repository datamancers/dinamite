#ifndef INSTRUMENTATION_H
#define INSTRUMENTATION_H

#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>

#include "binaryinstrumentation.h"

#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

const char *filename = "trace.bin";
static FILE *out = NULL;

int sockfd;
struct sockaddr_un claddr;
char *cl_path = "/home/work/llvm_log_client";
struct sockaddr_un sraddr;
char *sr_path = "/home/work/llvm_log_server";

//#define OPEN_LOG() if (unlikely(out == NULL)) { printf("Closing log file %s...\n", filename); out = fopen(filename,"wb"); }

#define BUFFER_SIZE 1024
#define SEND_SIZE (sizeof(logentry) * BUFFER_SIZE)

#define THREAD_ID() 1

static logentry entries[BUFFER_SIZE];
static int current = 0;

inline void insertOrSend(logentry *le) {
    if (current >= BUFFER_SIZE) {
        char *buf = (char*)entries;
        int sendIdx = 0;
        int retval = 0;
        while (sendIdx < SEND_SIZE) {
            retval = send(sockfd, buf + sendIdx, SEND_SIZE - sendIdx, 0);
            if (retval >= 0) {
                sendIdx += retval;
            }
        }
        current = 0;
    } 

    entries[current++] = *le;
}

inline void sendAll() {
    int sendSize = current * sizeof(logentry);
    char *buf = (char*)entries;
    int sendIdx = 0;
    int retval = 0;
    while (sendIdx < sendSize) {
        retval = send(sockfd, buf + sendIdx, sendSize - sendIdx, 0);
        if (retval >= 0) {
            sendIdx += retval;
        }
    }
    current = 0;
}

inline void fillFnLog(fnlog *fnl, char fn_event_type, int functionId) {
    fnl->thread_id = THREAD_ID();
    fnl->fn_event_type = fn_event_type;
    fnl->function_id = functionId;
}

inline void fillAccessLog(accesslog *acl, void *ptr, char value_type, value_store value, int type, int file, int line, int col, int typeId, int varId) {
    acl->thread_id = THREAD_ID();
    acl->ptr = ptr;
    acl->value_type = value_type;
    acl->value = value;
    acl->type = type;
    acl->file = file;
    acl->line = line;
    acl->col = col;
    acl->typeId = typeId;
    acl->varId = varId;
}

inline void fillAllocLog(alloclog *all, void *addr, uint64_t size, uint64_t num, int type, int file, int line, int col) {
    all->thread_id = THREAD_ID();
    all->addr = addr;
    all->size = size;
    all->num = num;
    all->type = type;
    all->file = file;
    all->line = line;
    all->col = col;
}

void logInit(int functionId) {
    sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        printf("Error opening socket\n");
    }
    claddr.sun_family = AF_UNIX;
    strcpy(claddr.sun_path, cl_path);
    sraddr.sun_family = AF_UNIX;
    strcpy(sraddr.sun_path, sr_path);

    int err;
    err = bind(sockfd, (struct sockaddr *)&claddr, sizeof(claddr));
    if (err < 0) {
        perror("bind");
    }
    err = connect(sockfd, (struct sockaddr *)&sraddr, sizeof(sraddr));
    if (err < 0) {
        perror("connect");
    }
}

void logExit(int functionId) {
    printf("Sending exit log");
    logentry le;
    le.entry_type = LOG_FN;
    fillFnLog(&(le.entry.fn), FN_BEGIN, -1);
    send(sockfd, &le, sizeof(logentry), 0);
    insertOrSend(&le);
    sendAll();

    unlink(cl_path);
    printf("Closing socket %s...\n", sr_path);
    close(sockfd);
}

void logFnBegin(int functionId) {
    logentry le;
    le.entry_type = LOG_FN;
    fillFnLog(&(le.entry.fn), FN_BEGIN, functionId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);
}

void logFnEnd(int functionId) {
    logentry le;
    le.entry_type = LOG_FN;
    fillFnLog(&(le.entry.fn), FN_END, functionId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);
}

void logAlloc(void *addr, uint64_t size, uint64_t num, int type, int file, int line, int col) {
    logentry le;
    le.entry_type = LOG_ALLOC;
    fillAllocLog(&(le.entry.alloc), addr, size, num, type, file, line, col);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);
}

void logAccessPtr(void *ptr, void *value, int type, int file, int line, int col, int typeId, int varId) {

    logentry le;
    le.entry_type = LOG_ACCESS;
    value_store vs;
    vs.ptr = value;
    fillAccessLog(&(le.entry.access), ptr, PTR, vs, type, file, line, col, typeId, varId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);


    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessStaticString(void *ptr, void *value, int type, int file, int line, int col, int typeId, int varId) {
}

void logAccessI8(void *ptr, uint8_t value, int type, int file, int line, int col, int typeId, int varId) {
    logentry le;
    le.entry_type = LOG_ACCESS;
    value_store vs;
    vs.i8 = value;
    fillAccessLog(&(le.entry.access), ptr, I8, vs, type, file, line, col, typeId, varId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);

    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessI16(void *ptr, uint16_t value, int type, int file, int line, int col, int typeId, int varId) {
    logentry le;
    le.entry_type = LOG_ACCESS;
    value_store vs;
    vs.i16 = value;
    fillAccessLog(&(le.entry.access), ptr, I16, vs, type, file, line, col, typeId, varId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);

    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessI32(void *ptr, uint32_t value, int type, int file, int line, int col, int typeId, int varId) {
    logentry le;
    le.entry_type = LOG_ACCESS;
    value_store vs;
    vs.i32 = value;
    fillAccessLog(&(le.entry.access), ptr, I32, vs, type, file, line, col, typeId, varId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);

    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessI64(void *ptr, uint64_t value, int type, int file, int line, int col, int typeId, int varId) {
    logentry le;
    le.entry_type = LOG_ACCESS;
    value_store vs;
    vs.i64 = value;
    fillAccessLog(&(le.entry.access), ptr, I64, vs, type, file, line, col, typeId, varId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);

    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

/* =============================
   These don't exist: */

void logAccessF8(void *ptr, uint8_t value, int type, int file, int line, int col, int typeId, int varId) {
    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessF16(void *ptr, uint16_t value, int type, int file, int line, int col, int typeId, int varId) {
    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

/* ============================= */

void logAccessF32(void *ptr, float value, int type, int file, int line, int col, int typeId, int varId) {
    logentry le;
    le.entry_type = LOG_ACCESS;
    value_store vs;
    vs.f32 = value;
    fillAccessLog(&(le.entry.access), ptr, F32, vs, type, file, line, col, typeId, varId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);

    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}

void logAccessF64(void *ptr, double value, int type, int file, int line, int col, int typeId, int varId) {
    logentry le;
    le.entry_type = LOG_ACCESS;
    value_store vs;
    vs.f64 = value;
    fillAccessLog(&(le.entry.access), ptr, F64, vs, type, file, line, col, typeId, varId);
    /*send(sockfd, &le, sizeof(logentry), 0);*/
    insertOrSend(&le);

    /*fprintf(out, "%p %llu %c %d %d %d %d %d\n", ptr,[> value, <]type, file, line, col, typeId, varId);*/
}
#endif
